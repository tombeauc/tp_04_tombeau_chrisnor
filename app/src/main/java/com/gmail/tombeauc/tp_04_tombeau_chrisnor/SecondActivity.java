package com.gmail.tombeauc.tp_04_tombeau_chrisnor;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SecondActivity  extends AppCompatActivity{
    private static final String TAG = "";
    private Button buttonClose;

    private void secondViews(Bundle savedInstanceState) {

        buttonClose = findViewById(R.id.close);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

    }
        @Override
        public void onStart()
        {
            super.onStart();
            Log.d(TAG, "La méthode onStart est appelée");

        }

        @Override
        public void onResume()
        {
            super.onResume();
            Log.d(TAG, "La méthode onResume est appelée");
        }

        @Override
        public void onPause()
        {
            super.onPause();
            Log.d(TAG, "La méthode onPause est appelée");
        }

        @Override
        public void onStop()
        {
            super.onStop();
            Log.d(TAG, "La méthode onStop est appelée");
        }

        @Override
        public void onRestart()
        {
            super.onRestart();
            Log.d(TAG, "La méthode onRestart est appelée");
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            Log.d(TAG, "La méthode onResume est appelée");
        }

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_second);
       secondViews(savedInstanceState);
       Log.d(TAG, "La méthode onCreate est appelée");
    }
}
